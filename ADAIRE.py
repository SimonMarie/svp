#############################################
####  Analyse Dynamique et Aérodynamique ####
####   de l'Intégration de tRajectoires #####
######## Pour l'Enseignement de la ##########
#########       mécanique     ###############
#########          du         ###############
#########         vol         ###############
#############################################
######## Version 1.0 / Fevrier 2020 #########
######## Version 2.0 / Avril   2023 #########
######## Version 2.4 / Fevrier 2024 #########
######## Version 3.0 / Octobre 2024 #########
#############################################
######### simon.marie@lecnam.net ############
#############################################
print("ADAIRE v3.0")        
from mpl_toolkits import mplot3d
import numpy as np
import matplotlib.pyplot as plt
import copy
#from numba import jit
import datetime
import copy
#from numba import jit
ft=0.3048     # Valeur de 1 pied en metre
kt=0.514444   # 1 noeud en m/s


class Avion:
    '''
    Class Plane

    Cette Class est la plus generique, elle inclu les methodes d integration et
    les constantes universelles.

    '''
    # Constantes 
    g0=9.81 # Acceleration de la pesenteur
    rho0=1.225    # Parametre atmospherique de la terre
    Z0=6700       # Parametre atmospherique de la terre
    ft=0.3048     # Valeur de 1 pied en metre
    kt=0.514444   # 1 noeud en m/s
    ENDC = '\033[m'
    BRED = '\033[31;1m'
    BBLUE = '\033[34;1m'
    BGRN  = '\033[32;1m'

    def __init__(self,z0=0.,u0=0.,name='Trajectoire_default.dat'):
        '''
        Init valeur par defaut pour le Cessna 182
        '''
        ### Initialisation de l'état initial
        self.W=np.zeros(13)
        #Vitesse init
        self.W[0]=u0            # u Vitesse initiale sur x (m/s)
        self.W[1]=0             # v Vitesse initiale sur y (m/s)
        self.W[2]=0             # w Vitesse initiale sur z (m/s)
        # Rotation init
        self.W[3]=0             # p Vitesse de roulis 
        self.W[4]=0             # q Vitesse de tanguage 
        self.W[5]=0             # r Vitesse de lacet 
        #Position initiale---------------
        self.W[6]=0             # X portee x
        self.W[7]=0             # Y portee y
        self.W[8]=-z0           # Z altitude
        #Orientation initiale---------------
        self.theta=0                 # Assiette longi
        self.phi=0                   # Assiette laterale (ou gite)
        self.psi=0                   # Cap (azimut) xAvion/Nord
        self.alpha=0.                # Incidence: Angle U.xAvion
        self.alpha_prev=0.           # Incidence: Angle U.xAvion
        self.beta=0.                 # Derapage : Angle U.yAvion
        self.beta_prev=0.            # Derapage : Angle U.yAvion
        self.gamma=0.                # Pente de montee
        self.xi=0.                   # Route Uxavion/Nord
        self.W[9:]=Euler2Quat([self.phi,self.theta,self.psi])
        self.W[9:]=NormalizeQuaternion(self.W[9:])
        self.Ufix=np.zeros(3)
        self.Ufix=Fixed2Body(self.W[:3],self.W[9:])
        self.nz=1
        #Commandes----------------------
        self.dm=0.                   # gouverne de profondeur (radian)
        self.dl=0                    # ailerons (radian)
        self.dn=0                    # gouverne de direction (radian)
        self.df=0                    # Coeff des volet (0 a 1)
        self.gaz=0.                  # Position manette des gaz (0<g<1)
        #Integration temporelle----------
        self.time=0.
        self.dt=0.01
        self.integ='rk4'
        #self.integ='euler'
        # Avion par defaut
        self.set_plane('cessna172')
        self.date=datetime.datetime.now()
        # Fichier de data
        self.display=True
        self.name=name
        self.fs=16.
        self.fic=open(self.name,'w')
        self.fic.close()


    def __repr__(self):
        '''
        Affichage des parametres de positions
        '''
        out=''
        if self.display:
            pos='('+str(self.W[6])+','+str(self.W[7])+','+str(self.W[8])+')'
            uav='('+str(self.W[0])+','+str(self.W[1])+','+str(self.W[2])+')'
            urel='('+str(self.Ufix[0])+','+str(self.Ufix[1])+','+str(self.Ufix[2])+')'
            out=self.BBLUE+'----   '+self.date.strftime('%d, %b %Y | %H:%M')+'   ----'+self.ENDC+'\n'
            out+='time= '+str(self.time)+'\n'
            out+=self.BGRN+'------------- Commandes -----------'+self.ENDC+'\n'
            out+='[dl,dm,dn] (°)  :'+str([self.dl*180/np.pi,self.dm*180/np.pi,self.dn*180/np.pi])+'\n'
            out+='Manette des gaz :'+str(self.gaz)+'\n'
            out+='Volets          :'+str(self.df)+'\n'
            out+=self.BRED+'----------- Aerodynamique ---------'+self.ENDC+'\n'
            out+='Trainee (Cx)  :'+str(self.Cx)+'\n'
            out+='Portance (Cz) :'+str(self.Cz)+'\n'
            out+='Finesse       :'+str(self.Cz/self.Cx)+'\n'
            out+=self.BRED+'----------- Accelerations ---------'+self.ENDC+'\n'
            out+='Reaction Sol           :'+str(self.Rsol)+'\n'
            out+='Facteur de charge (nz) :'+str(self.nz)+'\n'
            out+=self.BRED+'---------- Vitesses/terre ---------'+self.ENDC+'\n'
            out+='(u,v,w)         ='+urel+'\n'
            out+='pente gamma (°) ='+str(self.gamma*180/np.pi)+'\n'
            out+=self.BRED+'---------- Vitesses/avion ---------'+self.ENDC+'\n'
            out+='(u_a,v_a,w_a) ='+uav+'\n'
            out+='(p,q,r)       ='+str(self.W[3:6])+'\n'
            out+='Incidence ='+str(self.alpha*180/np.pi)+'° \n'
            out+='Derapage ='+str(self.beta*180/np.pi)+'° \n'
            out+=self.BRED+'------------- Position ------------'+self.ENDC+'\n'
            out+='Altitude  : '+str(-self.W[8]/self.ft)+' Pieds \n'
            out+='(x,y,z)     ='+pos+'\n'
            out+='Assiette    ='+str(self.theta*180/np.pi)+' ° \n'
            out+='Phi (gite)  ='+str(self.phi*180/np.pi)+' ° \n'
            out+='Cap         ='+str(self.psi*180/np.pi)+' ° \n'
            out+=self.BBLUE+'-----------------------------------'+self.ENDC+'\n'
            out+='\n\n'
        return out
    
    def set_plane(self,name):
        '''
        Change les parametre de l'apareil:
        cessna,a320,lionceau
        '''
        if name=='cessna172':
            #Geometrie:----------------------
            self.span=11                 # Envergure des 2 ailes (m)
            self.chord=1.5               # Corde moyenne (m) MAC
            #Aerodynamique-------------------
            self.Sref=self.span*self.chord
            self.pa=1.013e5              # pression atm au sol
            self.Cz0=0.307               # Portance init
            self.Cza=4.41                # Pente portance/incid
            self.Czdm=0.43               # Pente portance/braquage profondeur
            self.Czdf=0.5                # Pente portance/volet
            self.Czdq=3.9                # Pente portance/rotation q
            self.Cyb=-0.404              # Pente Coef lat / derapage
            self.Cydl=0                  # Pente Coef lat / aileron
            self.Cydn=0.187              # Pente Coef lat / gouv direction
            self.Cydp=-0.075             # Pente Coef lat / rotation p
            self.Cydr=0.214              # Pente Coef lat / rotation r
            self.Cx0=0.027               # Trainee init
            self.Cxa=0.121               # Pente traine/incid
            self.Cxdf=0.02               # Pente traine/volet
            self.Clb=-0.0923             # Pente Moment L/derapage
            self.Cldl=0.229              # Pente Moment L/ aileron
            self.Cldn=0.0147             # Pente Moment L/ direction
            self.Cldp=-0.484              # Pente Moment L/ rotation p
            self.Cldr=0.0798              # Pente Moment L/ rotation r
            self.Cm0=0.04                # Moment M init
            self.Cma=-0.613              # Pente Cm/apha 
            self.Cmdm=-1.2               # Pente moment Cm/braquage profondeur
            self.Cmdf=0.0                # Pente moment Cm/ volet
            self.Cmdq=-12.4              # Pente moment Cm/ rotation q
            self.Cnb=0.0587              # Pente Moment N/derapage
            self.Cndl=-0.0216            # Pente Cn/aileron
            self.Cndn=-0.0645            # Pente Cn/direction
            self.Cndp=-0.0278             # Pente Cn/ rotation p
            self.Cndr=-0.0937             # Pente Cn/ rotation r
            # Limite de decrochage -----------------------------------------
            self.alphaS=21.4*np.pi/180   # Incidence limite de décrochage
            self.betaS=16.7*np.pi/180    # Dérapage  limite de décrochage
            # Charge (masse) -----------------
            self.mv=1000.	             # Masse a vide (Kg)
            self.ms=200.	             # Masse utile  (Kg)
            self.m=self.ms+self.mv       # Masse totale (Kg)
            self.xcg=0.264               # Centre de gravite avion/chord moyenne
            self.xa=0.5                  # Foyer aero/chord moyenne
            Ixx=1285.3 # kg.m2
            Iyy=1825.
            Izz=2667
            Ixz=0.
            Ixy=0. # Plane symmetry
            Iyz=0. # PLane symmetry
            self.Inertie=np.array([[Ixx,Ixy,Ixz],[Ixy,Iyy,Iyz],[Ixz,Iyz,Izz]])
            # Inverse mass and inertia
            self.minv=1./self.m
            self.Iinv=np.linalg.inv(self.Inertie)
            # Propulsion ---------------------------------------------------
            self.get_COEFF()
            self.get_AERO(0)
            self.Pmot=2*self.m*9.81*self.Cx/self.Cz # Traction moteur (N)
            self.get_GROUND()
        if name=='boeing747':
            #Geometrie:----------------------
            self.span=60                 # Envergure des 2 ailes (m)
            self.chord=8.29              # Corde moyenne (m) MAC
            #Aerodynamique-------------------
            self.Sref=self.span*self.chord
            self.pa=1.013e5              # pression atm au sol
            self.Cz0=0.29               # Portance init
            self.Cza=4.4                # Pente portance/incid
            self.Czdm=0.32               # Pente portance/braquage profondeur
            self.Czdf=0.7                # Pente portance/volet
            self.Czdq=6.6                # Pente portance/rotation q
            self.Cyb=-0.404              # Pente Coef lat / derapage
            self.Cydl=0                  # Pente Coef lat / aileron
            self.Cydn=0.187              # Pente Coef lat / gouv direction
            self.Cydp=-0.075             # Pente Coef lat / rotation p
            self.Cydr=0.214              # Pente Coef lat / rotation r
            self.Cx0=0.0164              # Trainee init
            self.Cxa=0.2                 # Pente traine/incid
            self.Cxdf=0.02               # Pente traine/volet
            self.Clb=-0.160              # Pente Moment L/derapage
            self.Cldl=0.013              # Pente Moment L/ aileron
            self.Cldn=0.008              # Pente Moment L/ direction
            self.Cldp=-0.34              # Pente Moment L/ rotation p
            self.Cldr=0.13               # Pente Moment L/ rotation r
            self.Cm0=0.0                 # Moment M init
            self.Cma=-1.00               # Pente Cm/apha 
            self.Cmdm=-1.3               # Pente moment Cm/braquage profondeur
            self.Cmdf=-2.7               # Pente moment Cm/ volet
            self.Cmdq=-20.5              # Pente moment Cm/ rotation q
            self.Cnb=0.160               # Pente Moment N/derapage
            self.Cndl=-0.0018            # Pente Cn/aileron
            self.Cndn=-0.1               # Pente Cn/direction
            self.Cndp=-0.026             # Pente Cn/ rotation p
            self.Cndr=-0.280             # Pente Cn/ rotation r
            # Limite de decrochage -----------------------------------------
            self.alphaS=12.2*np.pi/180   # Incidence limite de décrochage
            self.betaS=6.7*np.pi/180    # Dérapage  limite de décrochage
            # Charge (masse) -----------------
            self.mv=228773.	             # Masse a vide (Kg)
            self.ms=8000.	             # Masse utile  (Kg)
            self.m=self.ms+self.mv       # Masse totale (Kg)
            self.xcg=0.25                # Centre de gravite avion/chord moyenne
            self.xa=0.5                  # Foyer aero/chord moyenne
            Ixx=24676819. # kg.m2
            Iyy=44879270.
            Izz=67386699.
            Ixz=1315193.
            Ixy=0. # Plane symmetry
            Iyz=0. # PLane symmetry
            self.Inertie=np.array([[Ixx,Ixy,Ixz],[Ixy,Iyy,Iyz],[Ixz,Iyz,Izz]])
            # Inverse mass and inertia
            self.minv=1./self.m
            self.Iinv=np.linalg.inv(self.Inertie)
            # Propulsion ---------------------------------------------------
            self.get_COEFF()
            self.get_AERO(0)
            self.Pmot=2*self.m*9.81*self.Cx/self.Cz # Traction moteur (N)
            self.get_GROUND()



    def trans_terre_avion(self):
        '''
        Matrice de passage du repere terrestre au repere avion
        '''
        R1=-np.sin(self.psi)*np.cos(self.phi)+np.cos(self.psi)*np.sin(self.theta)*np.sin(self.phi)
        R2=np.sin(self.psi)*np.sin(self.phi)+np.cos(self.psi)*np.sin(self.theta)*np.cos(self.phi)
        R3=np.cos(self.psi)*np.cos(self.phi)+np.sin(self.psi)*np.sin(self.theta)*np.sin(self.phi)
        R4=-np.cos(self.psi)*np.sin(self.phi)+np.sin(self.psi)*np.sin(self.theta)*np.cos(self.phi)
        Rta=np.array([\
                [np.cos(self.psi)*np.cos(self.theta),np.sin(self.psi)*np.cos(self.theta),-np.sin(self.theta)],\
                [R1,R3,np.cos(self.theta)*np.sin(self.phi)],\
                [R2,R4,np.cos(self.theta)*np.cos(self.phi)]\
                ])
        return Rta.T


    def trans_rot(self):
        '''
        Matrice de passage du repere terrestre au repere avion pour les angles
        d'Euler
        '''
        Rtrot=np.array([\
                [1,np.sin(self.phi)*np.tan(self.theta),np.cos(self.phi)*np.tan(self.theta)],\
                [0.,np.cos(self.phi),-np.sin(self.phi)],\
                [0.,np.sin(self.phi)/np.cos(self.theta),np.cos(self.phi)/np.cos(self.theta)]\
                ])
        return Rtrot

    def trans_aero_avion(self):
        '''
        Matrice de passage du repere aero au repere avion
        X_avion=Rarav * X_aero
        '''
        malpha=-self.alpha
        Rarav=np.array([ \
            [np.cos(malpha)*np.cos(self.beta),-np.cos(malpha)*np.sin(self.beta),-np.sin(malpha)], \
            [np.sin(self.beta),np.cos(self.beta),0],\
            [np.sin(malpha)*np.cos(self.beta),-np.sin(malpha)*np.sin(self.beta),np.cos(malpha)]\
            ])
        return Rarav.T

    def getchar(self):
        '''
        Calcul des grandeurs caracteristiques de l'avion
        '''
        self.uchar=np.sqrt(2*self.m*self.g0/(self.rho0*self.Sref*self.Cz))

    def get_GROUND(self):
        '''
        Calcul la reaction du sol si besoin
        '''
        self.get_gravity()
        P=self.m*self.gz
        if (self.W[8]>=0 and abs(self.aeroz)<abs(P)):
            self.Rsol=(abs(P)-abs(self.aeroz))
            self.W[8]=0
        else:
            self.Rsol=0


    def get_gravity(self):
        '''
        Calcul du poid dans le repere avion
        '''
        q0 = self.W[9]
        qx = self.W[10]
        qy = self.W[11]
        qz = self.W[12]
        self.gx=2*self.g0*(qx*qz-qy*q0)
        self.gy=2*self.g0*(qy*qz+qx*q0)
        self.gz=self.g0*(qz*qz+q0*q0-qx*qx-qy*qy)


    def get_incidence(self):
        '''
        Met a jour les valeurs d'incidence alpha et beta
        Theta=gamma+alpha pour une position standard.
        Phi=xi+beta pour une position standard.
        '''
        if self.Ufix[0]>0:
            self.Ut=np.sqrt(self.Ufix[0]**2+self.Ufix[1]**2+self.Ufix[2]**2)
            alpha1=np.arctan2(self.Ufix[2],self.Ufix[0])
            beta1=np.arcsin(self.Ufix[1]/self.Ut)
            if alpha1!=0:
                sgn=alpha1/abs(alpha1)
            else:
                sgn=1
            #self.beta=np.arcsin(self.Ufix[1],self.Ut)
            #self.beta=np.arctan2(self.Ufix[1],self.Ufix[0])
            alpha2=self.gamma-self.theta
            beta2=self.phi-self.xi
            #self.alpha=sgn*min(abs(alpha1),abs(alpha2))
            self.alpha=alpha1
            self.beta=beta1
            #self.alpha=0
            #self.beta=0

    def momeq(self,alpha=0):
        '''
        Calcul la profondeur pour annuler le moment en tanguage
        a une incidence donnée
        '''
        CmA=self.Cm0+self.Cma*alpha
        self.dm=-CmA/self.Cmdm
        self.get_COEFF()

    def get_COEFF(self):
        '''
        Met a jour les parametres aero dans le repere avion
        Modele Aero
        '''
        # Extract state
        u = self.W[0]
        v = self.W[1]
        w = self.W[2]
        p = self.W[3]
        q = self.W[4]
        r = self.W[5]
        V=(u*u+v*v+w*w)**0.5
        if V==0:
            Vinv=0.
        else:
            Vinv=1./V
        # Traine induite:
        self.lmda=self.span/self.chord
        ki=1/(np.pi*0.8*self.lmda)
        # Coeff aero
        self.Cz=self.Cz0+self.Cza*self.alpha+self.Czdf*self.df+self.Czdm*self.dm+0.5*self.Czdq*q*self.chord*Vinv
        self.Cx=self.Cx0+ki*self.Cz**2+self.Cxdf*self.df+self.Cxa*self.alpha
        self.Cy=self.Cyb*self.beta+0.5*self.Cydp*p*self.chord*Vinv+0.5*self.Cydr*r*self.chord*Vinv

        # Coeff des moments aero:
        self.Cl=self.Clb*self.beta+self.Cldl*self.dl+self.Cldn*self.dn+0.5*self.Cldp*p*self.chord*Vinv + self.Cldr*r*self.chord*Vinv 
        self.Cm=self.Cm0+self.Cma*self.alpha+self.Cmdm*self.dm+self.Cmdq*q*self.chord*Vinv
        #self.Cm=self.Cm0+self.Cza*(self.xcg-self.xa)*self.alpha+self.Cmdm*self.dm
        self.Cn=self.Cnb*self.beta+self.Cndn*self.dn+self.Cndl*self.dl+0.5*self.Cndp*p*self.chord*Vinv + self.Cndr*r*self.chord*Vinv 

        self.check_decroch() # correction decrochage

    def check_decroch(self):
            '''
            Stall model taken from Goates 2021
            https://digitalcommons.usu.edu/cgi/viewcontent.cgi?article=1025&context=mae_stures
            Used in MachUpX (https://github.com/usuaero/MachUpX)
            '''
            C_a = np.cos(self.alpha)
            S_a = np.sin(self.alpha)
            C_B = np.cos(self.beta)
            S_B = np.sin(self.beta)

            # Get stall values
            CzS = 2.0*S_a*S_a*C_a*np.sign(self.alpha)
            CxS = 1-C_a*S_a*np.sign(self.alpha)
            CmS = -0.5*S_a
            CyS = 0.2*S_B*S_B*C_B*np.sign(self.beta)
            ClS = -CyS*0.05
            CnS = CyS*0.1

            # Blending functions
            k=100 # Blend coeff
            Coeff_Longi = 1/(1+np.exp(-k*(-self.alphaS-self.alpha)))+1/(1+np.exp(-k*(self.alpha-self.alphaS)))
            Coeff_Lat = 1/(1+np.exp(-k*(-self.betaS-self.beta)))+1/(1+np.exp(-k*(self.beta-self.betaS)))

            # Blend
            self.Cz = self.Cz*(1-Coeff_Longi)+CzS*Coeff_Longi  
            self.Cx = self.Cx*(1-Coeff_Longi)+CxS*Coeff_Longi
            self.Cm = self.Cm*(1-Coeff_Longi)+CmS*Coeff_Longi
            self.Cy = self.Cy*(1-Coeff_Lat)+CyS*Coeff_Lat 
            self.Cl = self.Cl*(1-Coeff_Lat)+ClS*Coeff_Lat
            self.Cn = self.Cn*(1-Coeff_Lat)+CnS*Coeff_Lat


    def run(self,ntmax=10000,dt=1e-3):
        '''
        Avance la trajectoire de ntmax iterations ou jusqu a z=0 
        '''
        self.dt=dt
        self.fic=open(self.name,'a')
        nt=0 # Iterations pour 1 run: =0 au debut de chaque execution
        while (nt<=ntmax and self.W[8]<=0):
            self.time+=dt
            nt+=1
            self.trajint()
            self.write()        
        self.fic.close() 
        self.date+=datetime.timedelta(seconds=dt*nt)
        print(self)

    def get_controls(self,t):
        '''
        Get the control vector dl,dm,dn,dx,df
        TODO : Brancher cette routine a PyGame pour mode Simulateur Interactif
        Cette routine ne fait rien pour le moment les valeurs sont inchangées
        '''
        #self.dm=0.                   # gouverne de profondeur (radian)
        #self.dl=0                    # ailerons (radian)
        #self.dn=0                    # gouverne de direction (radian)
        #self.df=0                    # Coeff des volet (0 a 1)
        #self.gaz=0.                  # Position manette des gaz (0<g<1)
        

    def get_AERO(self,t):
        '''
        Compute and returns aerodynamic forces and moments.
        '''

        # Get control state
        self.get_controls(t)

        # Declare force and moment vector
        AERO = np.zeros(6)

        # Get states
        rho = self.rho()

        u = self.W[0]
        v = self.W[1]
        w = self.W[2]
        p = self.W[3]
        q = self.W[4]
        r = self.W[5]
        V = np.sqrt(u*u+v*v+w*w)
        if V==0:
            V_inv=0.
        else:
            V_inv = 1.0/V
        self.alpha = np.arctan2(w,u)
        self.beta = np.arcsin(v*V_inv)
        const = 0.5*V_inv
        p_bar = self.span*p*const
        q_bar = self.chord*q*const
        r_bar = self.span*r*const
        u_inf = self.W[0:3]*V_inv

        # Get Alpha _ point
        self.alpha_dot = 0.5*self.chord*V_inv*(self.alpha-self.alpha_prev)/self.dt
        self.beta_dot  = 0.5*self.span*V_inv*(self.beta-self.beta_prev)/self.dt

        # Store for next evaluation
        self.alpha_prev = self.alpha
        self.beta_prev = self.beta

        # Update Aero Coeff
        self.get_COEFF()

        # Get redimensionalizer
        redim = 0.5*rho*V*V*self.Sref

        # Get trig vals
        C_a = np.cos(self.alpha)
        S_a = np.sin(self.alpha)
        #S_a = w/u*C_a
        C_B = np.cos(self.beta)
        S_B = v*V_inv

        # Bilan dans repere Aero
        AERO[0] = redim*(self.Cz*S_a-self.Cy*C_a*S_B-self.Cx*C_a*C_B)
        AERO[1] = redim*(self.Cy*C_B-self.Cx*S_B)
        AERO[2] = redim*(-self.Cz*C_a-self.Cy*S_a*S_B-self.Cx*S_a*C_B)
        AERO[3] = redim*self.Cl*self.span
        AERO[4] = redim*self.Cm*self.chord
        AERO[5] = redim*self.Cn*self.span
        
        self.aeroz=AERO[2]

        return AERO


    def get_THRUST(self,t):
        '''
        Propulsion Model:
        Met a jour la propulsion dans le repere avion
        La loi utilisée est de type:
        F=k*rho*V^l * dx
        Avec l=-1 => Helice
             l=0  => Turborec
             l=1  => Turborec post comb
             l=2  => Statoreacteur
             dx   => Manette des gaz (0<dx<1)
             k    => Cste moteur

        return: ndarray :: Fx Fy Fz, L M N
        '''
        THRUST=np.zeros(6)
        self.kf=1.43e5
        self.Prop=self.gaz*self.Pmot*self.rho()/self.rho0
        THRUST[0]=self.Prop
        THRUST[1]=0
        THRUST[2]=0
        THRUST[3]=0.
        THRUST[4]=0.
        THRUST[5]=0.

        return THRUST


    def dWdt(self,W, t):
        """Calculates the derivative of the state vector with respect to time
        at the current state and time.

        Parameters
        ----------
        W : State Vector
            0 - u
            1 - v
            2 - w
            3 - p
            4 - q
            5 - r
            6 - x
            7 - y
            8 - z
            9 - q0
           10 - qx
           11 - qy
           12 - qz
        t : float
            Current simulation time.

        Returns
        -------
        ndarray
            Time rate of change of each state variable.
        """

        # Get aerodynamic forces and moments
        AERO   = self.get_AERO(t)
        # Get propulsion forces and moments
        THRUST = self.get_THRUST(t)
        
        # Reaction du sol si au sol
        self.get_GROUND()

        # Extract state
        u = self.W[0]
        v = self.W[1]
        w = self.W[2]
        p = self.W[3]
        q = self.W[4]
        r = self.W[5]
        q0 = self.W[9]
        qx = self.W[10]
        qy = self.W[11]
        qz = self.W[12]
        Ixx,Iyy,Izz,Ixz=self.Inertie[0,0],self.Inertie[1,1],self.Inertie[2,2],self.Inertie[0,2]
        Ixy=0.
        Iyz=0.
        # Apply Newton's equations
        dW = np.zeros(13)
        
        # Linear acceleration
        dW[0] = 2*self.g0*(qx*qz-qy*q0) + (AERO[0]+THRUST[0])*self.minv + r*v-q*w
        dW[1] = 2*self.g0*(qy*qz+qx*q0) + (AERO[1]+THRUST[1])*self.minv + p*w-r*u
        dW[2] =  self.g0*(qz*qz+q0*q0-qx*qx-qy*qy) + self.minv*(-self.Rsol +AERO[2]+THRUST[2]) + q*u-p*v

        self.nz=dW[2]/self.g0    
        self.nx=dW[0]/self.g0    
        self.ny=dW[1]/self.g0    

        # Angular acceleration
        pq = p*q
        qr = q*r
        pr = p*r
        p2 = p*p
        q2 = q*q
        r2 = r*r
        M = [0.0, 0.0, 0.0]
        # Bilan des moments
        M[0] =  AERO[3] + THRUST[3] + (Iyy-Izz)*qr + Iyz*(q2-r2)+Ixz*pq-Ixy*pr
        M[1] =  AERO[4] + THRUST[4] + (Izz-Ixx)*pr + Ixz*(r2-p2)+Ixy*qr-Iyz*pq
        M[2] =  AERO[5] + THRUST[5] + (Ixx-Iyy)*pq + Ixy*(p2-q2)+Iyz*pr-Ixz*qr

        # Omega = Iinv * Moments
        dW[3] = self.Iinv[0,0]*M[0] + self.Iinv[0,1]*M[1] + self.Iinv[0,2]*M[2]
        dW[4] = self.Iinv[1,0]*M[0] + self.Iinv[1,1]*M[1] + self.Iinv[1,2]*M[2]
        dW[5] = self.Iinv[2,0]*M[0] + self.Iinv[2,1]*M[1] + self.Iinv[2,2]*M[2]

        # Translation dans le repere terrestre
        dW[6:9] = Body2Fixed(self.W[:3], self.W[9:])

        # Rotation (dynamic quaternion equation: Qdot=1/2 Omega ^ Q)
        dW[9] = 0.5*(-qx*p-qy*q-qz*r)
        dW[10] = 0.5*(q0*p-qz*q+qy*r)
        dW[11] = 0.5*(qz*p+q0*q-qx*r)
        dW[12] = 0.5*(-qy*p+qx*q+q0*r)

        return dW

    def get_attitude(self):
        '''
        return Euler angles of plane 
        phi theta psi
        '''
        self.phi,self.theta,self.psi=Quat2Euler(self.W[9:])

    def euler(self):
        '''
        Integration trajectoire d'ordre 1 du vercteur d'Etat W
        W[0,1,2]      = Ux Uy Uz
        W[3,4,5]      = p,q,r
        W[6,7,8]      = Position X Y Z
        W[9,10,11,12] = Quaternion q0 qx qy qz
        '''
        self.W+=self.dt*self.dWdt(self.W,self.time) 


    def rk2(self):
        '''
        Integration trajectoire d'ordre 1 du vercteur d'Etat W
        W[0,1,2]      = Ux Uy Uz
        W[3,4,5]      = p,q,r
        W[6,7,8]      = Position X Y Z
        W[9,10,11,12] = Quaternion q0 qx qy qz
        '''
        W0=copy.copy(self.W)
        k1=self.dWdt(W0,self.time)
        self.W=W0+0.5*self.dt*k1
        k2=self.dWdt(self.W,self.time+0.5*self.dt) 
        self.W=W0+self.dt*k2

    def rk4(self):
        '''
        Integration trajectoire d'ordre 1 du vercteur d'Etat W
        W[0,1,2]      = Ux Uy Uz
        W[3,4,5]      = p,q,r
        W[6,7,8]      = Position X Y Z
        W[9,10,11,12] = Quaternion q0 qx qy qz
        '''
        W0=copy.copy(self.W)
        k1=self.dWdt(W0,self.time)
        self.W=W0+0.5*self.dt*k1
        k2=self.dWdt(self.W,self.time+0.5*self.dt) 
        self.W=W0+0.5*self.dt*k2
        k3=self.dWdt(self.W,self.time+0.5*self.dt) 
        self.W=W0+self.dt*k3
        k4=self.dWdt(self.W,self.time+self.dt) 
        self.W=W0+0.1666666666666666667*self.dt*(k1+2.*k2+2.*k3+k4)

    #@jit
    def trajint(self):
        '''
        Permet d integrer les equations du mouvement dans le 
        repere geocentrique (x,y,z)

        * Schema d Euler (Ordre 1)
        * Schema RK2 (Ordre 2)
        * Schema RK4 (Ordre 4)
        '''
        # In case of user modification of Euler Angles
        self.W[9:]=Euler2Quat([self.phi,self.theta,self.psi])
        self.W[9:]=NormalizeQuaternion(self.W[9:])
        if self.integ=='euler':
            self.euler()
        elif self.integ=='rk2':
            self.rk2()
        elif self.integ=='rk4':
            self.rk4()
        else:
            print(self.integ,' Integrateur non valide')
        self.W[9:]=NormalizeQuaternion(self.W[9:])
        self.get_attitude()
        self.Ufix=Body2Fixed(self.W[:3],self.W[9:])
        self.gamma=np.arctan2(self.Ufix[2],self.Ufix[0])

    def write(self):
        '''
        Ecriture de la trajectoire

        "0-time | 1-x | 2-y | 3-z | 4-u | 5-v | 6-w | 7-alpha | 8-beta | 9-theta
       | 10-psi | 11-Ax | 12-Ay | 13-Az | 14-Phi | 15-nz | 16-nx | 17-ny | 18-Cx
       | 19-Cz | 20-Prop "
        '''
        self.fic.write('%.4f %.4f %.4f %.4f %.6f %.6f %.6f %.4f %.4f %.4f %.4f\
                %.4f %.4f %.4f %.4f %.4f %.4f %.4f %.4f %.4f %.4f\n'%(self.time,self.W[6],\
                self.W[7],-self.W[8],self.Ufix[0],self.Ufix[1],self.Ufix[2],self.alpha,\
                self.beta,self.theta,self.psi,self.gx,self.gy,self.gz,\
                self.phi,self.nz,self.nx,self.ny,self.Cx,self.Cz,self.Prop))

    def load(self):
        '''
        Chargement des resultats:
        '''
        print("0-time | 1-x | 2-y | 3-z | 4-u | 5-v | 6-w | 7-alpha | 8-beta |\
                9-theta | 10-cap | 11-13 Acc | 14-phi | 15-nz | 16-nx | 17-ny | 18-Fd | 19-Fl | 20-Prop ")
        return np.loadtxt(self.name,dtype='f')


    def rho(self):
        '''
        Modele d atmosphere
        '''
        T=288.15-0.0065*np.abs(self.W[8])
        return self.rho0*(T/288.15)**(1+(-9.81/(-0.0065*287)))
        #return self.rho0*np.exp(-np.abs(self.Pos[2])/6400) 


    def plotraj(self,kind='2dxz',impref=''):
        '''
        Represente la trajectoi67.17310597457337, 0.60366716re a partir des resultat de l integration 
        de la trajectoire.

        * kind='rect' (default) => Dans le plan (x,z)
        * kind='polar'          => Dans le plan (r,theta)
        '''
        data=self.load()
        if kind=='2dxz':
            plt.plot(data[:,1],data[:,3]);
            plt.xlabel('Portee (m)');plt.ylabel('Alt (m)');
            plt.grid(True)
        if kind=='2dxy':
            plt.plot(data[:,1],data[:,2]);
            plt.xlabel('Trace X (m)');plt.ylabel('Trace Y (m)');
            plt.grid(True)
        if kind=='2dyz':
            plt.plot(data[:,2],data[:,3]);
            plt.xlabel('Trace Y (m)');plt.ylabel('Alt (pied)');
            plt.grid(True)
        elif kind=='3d':
            ax=plt.axes(projection='3d')
            ax.plot3D(data[:,1],data[:,2],data[:,3])


    def anim(self,skip=1):
        '''
        Anim la trajectoire.
        '''
        fig=plt.figure(figsize=(12,12),dpi=120)
        traj=self.load()
        print(str(len(traj)/skip)+' images')
        ii=0
        for data in traj[0:-1:skip]:
            ii+=1
            plt.polar(np.linspace(0,2.*np.pi,100),self.Rt*np.ones(100)/1000.,'r')
            plt.polar(np.linspace(0,2.*np.pi,100),self.earth_lim1*np.ones(100)/1000.,'--b')
            plt.polar(data[2],data[1]/1000,'gs')
            plt.plot(0.5*np.pi+2.*np.pi*data[0]/self.TL,self.al/1000,'oy')
            fig.savefig('tmp/img_'+str(ii)+'.png',bbox_inches='tight')
            plt.clf()

    def monit(self,fig):
        '''
        Represente l evolution des donnees trajectoire.
        Cette methode necessite un objet matplolib (fig) en input.

        * 11 :U(t)    12 :Acc(U)
        '''
        fs=self.fs
        nline=5
        ncol=1
        T=self.load()
        fig.add_subplot(nline,ncol,1)
        plt.plot(T[:,0]/60,T[:,4],'r',label='u');plt.grid(True)
        plt.ylabel('$U_x (m/s)$',fontsize=fs);
        fig.add_subplot(nline,ncol,2)
        plt.plot(T[:,0]/60,T[:,6],'b',label='w');plt.grid(True)
        plt.xlabel('time (min)',fontsize=fs);plt.ylabel('$U_z (m/s)$',fontsize=fs)
        fig.add_subplot(nline,ncol,3)
        plt.plot(T[:,0]/60,T[:,16],'r',label='nx');
        #plt.plot(T[:,0]/60,T[:,17],'g',label='ny');
        plt.plot(T[:,0]/60,T[:,15],'b',label='nz');plt.grid(True)
        plt.legend(fontsize=fs)
        plt.xlabel('time (min)',fontsize=fs);plt.ylabel('Charges',fontsize=fs);
        fig.add_subplot(nline,ncol,4)
        #plt.plot(T[:,0]/60,T[:,10]*180/np.pi,'r',label='$\Phi$');
        plt.plot(T[:,0]/60,T[:,9]*180/np.pi,'g',label='$\\theta$');
        #plt.plot(T[:,0]/60,T[:,14]*180/np.pi,'b',label='$\Psi$');
        plt.plot(T[:,0]/60,T[:,7]*180/np.pi,'y',label='$\\alpha$');
        #plt.plot(T[:,0]/60,T[:,8]*180/np.pi,'m',label='$\\beta$')
        plt.grid(True)
        plt.legend(fontsize=fs)
        plt.xlabel('time (min)',fontsize=fs);plt.ylabel('Angles (°)',fontsize=fs);
        fig.add_subplot(nline,ncol,5)
        plt.plot(T[:,0]/60,T[:,18],'r',label='$C_D$');
        plt.plot(T[:,0]/60,T[:,19],'k',label='$C_L$');
        plt.grid(True)
        plt.legend(fontsize=fs)
        plt.xlabel('time (min)',fontsize=fs);plt.ylabel('Coeffs Aero',fontsize=fs)


def QuatMult(q0, q1):
    q00 = q0[0]
    q0x = q0[1]
    q0y = q0[2]
    q0z = q0[3]
    q10 = q1[0]
    q1x = q1[1]
    q1y = q1[2]
    q1z = q1[3]

    return [q00*q10-q0x*q1x-q0y*q1y-q0z*q1z, q00*q1x+q0x*q10+q0y*q1z-q0z*q1y, q00*q1y-q0x*q1z+q0y*q10+q0z*q1x, q00*q1z+q0x*q1y-q0y*q1x+q0z*q10]

def Euler2Quat(E):
    phi = 0.5*E[0]
    theta = 0.5*E[1]
    psi = 0.5*E[2]
    C_phi = np.cos(phi)
    S_phi = np.sin(phi)
    C_theta = np.cos(theta)
    S_theta = np.sin(theta)
    C_psi = np.cos(psi)
    S_psi = np.sin(psi)
    CphCth = C_phi*C_theta
    SthSps = S_theta*S_psi
    SthCps = S_theta*C_psi
    SphCth = S_phi*C_theta
    return [CphCth*C_psi+S_phi*SthSps, SphCth*C_psi-C_phi*SthSps, C_phi*SthCps+SphCth*S_psi, CphCth*S_psi-S_phi*SthCps]

def NormalizeQuaternion(q):
    q0 = q[0]
    q1 = q[1]
    q2 = q[2]
    q3 = q[3]
    m = np.sqrt(q0*q0+q1*q1+q2*q2+q3*q3)
    return [e/m for e in q]

def NormalizeQuaternionNearOne(q):
    q0 = q[0]
    q1 = q[1]
    q2 = q[2]
    q3 = q[3]
    m = 1.5-0.5*(q0*q0+q1*q1+q2*q2+q3*q3)
    return [e*m for e in q]

def Quat2Euler(q):
    q0 = q[0]
    q1 = q[1]
    q2 = q[2]
    q3 = q[3]
    x = q0*q2-q1*q3
    if x != 0.5 and x != -0.5:
        q02 = q0*q0
        qx2 = q1*q1
        qy2 = q2*q2
        qz2 = q3*q3
        return [np.arctan2(2*(q0*q1+q2*q3), q02+qz2-qx2-qy2), np.arcsin(2*x),np.arctan2(2*(q0*q3+q1*q2), q02+qx2-qy2-qz2)]
    else:
        return [2.*np.arcsin(q1*1.4142135623730951), np.pi*x, 0.]

def Body2Fixed(v, q):
    q0 = q[0]
    q1 = q[1]
    q2 = q[2]
    q3 = q[3]
    q00 = q0*q0
    qxx = q1*q1
    qyy = q2*q2
    qzz = q3*q3
    q0x = 2*q0*q1
    q0y = 2*q0*q2
    q0z = 2*q0*q3
    qxy = 2*q1*q2
    qxz = 2*q1*q3
    qyz = 2*q2*q3
    v0 = v[0]
    v1 = v[1]
    v2 = v[2]
    return [(qxx+q00-qyy-qzz)*v0 + (qxy-q0z)*v1 + (qxz+q0y)*v2, (qxy+q0z)*v0 + (qyy+q00-qxx-qzz)*v1 + (qyz-q0x)*v2, (qxz-q0y)*v0 + (qyz+q0x)*v1 + (qzz+q00-qxx-qyy)*v2]

def Fixed2Body(v, q):
    q0 = q[0]
    q1 = q[1]
    q2 = q[2]
    q3 = q[3]
    q00 = q0*q0
    qxx = q1*q1
    qyy = q2*q2
    qzz = q3*q3
    q0x = 2*q0*q1
    q0y = 2*q0*q2
    q0z = 2*q0*q3
    qxy = 2*q1*q2
    qxz = 2*q1*q3
    qyz = 2*q2*q3
    v0 = v[0]
    v1 = v[1]
    v2 = v[2]
    return [(qxx+q00-qyy-qzz)*v0 + (qxy+q0z)*v1 + (qxz-q0y)*v2, (qxy-q0z)*v0 + (qyy+q00-qxx-qzz)*v1 + (qyz+q0x)*v2, (qxz+q0y)*v0 + (qyz-q0x)*v1 + (qzz+q00-qxx-qyy)*v2]
