ADAIRE 

# Analyse Dynamique et Aérodynamique de l'Intégration de tRajectoire pour l'Enseignement de la mécaniqe du vol.
simon.marie@cnam.fr

# Utilisation:

import ADAIRE as ada

avion=ada.Avion(z0,u0)

z0: Altitude initiale (en m)
U0: Vitesse initiale (Ref Avion en m/s)


# Integration
avion.run(dt,maxiter)
dt: pas de temps
itermax: Nb d'iterations max

